$( document ).ready(function() {
    ymaps.ready(function () {
        const myMap = new ymaps.Map("YMapsID", {
            center: [41.311280, -72.931280],
            controls: ['zoomControl'],
            zoom: 18,
        });
        const myPlacemark = new ymaps.Placemark([41.311280, -72.931280], {}, {
            preset: 'islands#redIcon'
        });
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
    });
    $(function(){
        $('#form').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                },
                phone: {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: "Поле 'Имя' обязательно к заполнению",
                    minlength: "Введите не менее 3-х символов в поле 'Имя'"
                },
                phone: {
                    required: "Поле 'Телефон' обязательно к заполнению",
                }
            },
        });
    });
    $('#formRadio').validate({
       rules: {
           radio2: {
               required: true,
           }
       }
    });
    $('.mask').inputmask();

    $('#form').on('submit', (e) => {
        e.preventDefault();
        setTimeout(() => {
            if(!$('#name').hasClass('error') && !$('#phone').hasClass('error')) {
                UIkit.switcher($('.switcher-wrapper')).show(1);
            }
        }, 100);
    });

    $('#formRadio').on('submit', (e) => {
        e.preventDefault();
        setTimeout(() => {
            if(!$('#formRadio input').hasClass('error')) {
                UIkit.switcher($('.switcher-wrapper')).show(2);
            }
        })
    });

    $('#name').on('keypress', function() {
        var that = this;
        setTimeout(function() {
            var res = /[^а-яА-ЯёЁa-zA-Z ]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        }, 0);
    });
});
